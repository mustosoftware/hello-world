import java.util.Scanner;
public class RabbitHouse {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);

		String mode = input.next();
		String name = input.next();
		
		if (mode.equals("normal")) System.out.println(rabbitNormal(name.length()));
		else if (mode.equals("palindrome")) System.out.println(rabbitPalindrome(name));
		
	}
	
	static int rabbitNormal (int lenName)
	{
		if (lenName == 1) return 1;
		else
		{
			return 1 + lenName*rabbitNormal(lenName-1);
		}
	}

	public static boolean isPalindrome(String str)
	{
		boolean result = true;
		int lenStr = str.length();

		for (int i = 0; i<(lenStr/2) && result; i++)
		{
			if (str.charAt(i) != str.charAt(lenStr-i-1)) result = false;
		}
		return result;
	}

	static int rabbitPalindrome (String name)
	{
            int lenName = name.length();
            int sumChild = 0;
            if (lenName == 1) return 0;
            else
                if (isPalindrome(name))
                    return 0;
                else
                {
                    sumChild++;
                    for (int x = 0; x<lenName; x++)
                    {
                        sumChild += rabbitPalindrome(name.substring(0, x) + name.substring(x+1));
                    }
                }
            
            return sumChild;
	}
}

