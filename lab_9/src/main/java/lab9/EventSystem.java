package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.Date;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Class representing event managing system
 */
public class EventSystem {
    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        Date starTime = formatter.parse(startTimeStr);
        Date endTime = formatter.parse(endTimeStr);
        //yyyy-MM-dd_HH:mm:ss
        if (!checkEvent(name)) {

            if (endTime.before(starTime)) {
                return "Waktu yang diinputkan tidak valid!";
            } else {
                Event newEvent = new Event(name, starTime, endTime, costPerHourStr);
                this.events.add(newEvent);

                return "Event " + name + " berhasil ditambahkan!";
            }
        } else {
            return "Event " + name + " sudah ada!";
        }
    }

    public String addUser(String name) {
        if (!checkUser(name)) {
            User u = new User(name);
            return String.format("User %s berhasil ditambahkan!", name);
        } else {
            return String.format("User %s sudah ada! ", name);
        }
    }

    public String registerToEvent(String userName, String eventName) {
        // TODO: Implement
        if (!checkUser(userName) && !checkEvent(eventName)) {
            return String.format("Tidak ada pengguna dengan nama %s dan acara dengan nama %s!", userName, eventName);
        } else if (!checkUser(userName)) {
            return String.format("Tidak ada pengguna dengan nama %s!", userName);
        } else if (!checkEvent(eventName)) {
            return String.format("Tidak ada acara dengan nama %s!", eventName);
        } else {
            User u = getUserByString(userName);
            Event e = getEventByString(eventName);

            if (checkAvailableTime(u, e)) {
                u.addEvent(e);
                return String.format("%s berencana menghadiri %s!", userName, eventName);
            } else
                return String.format("%s sibuk sehingga tidak dapat menghadiri %s!", userName, eventName);
        }
    }

    /*
        Check whether user with name findStr is in this.Users
     */
    private boolean checkUser(String findStr) {
        for (User u : this.users) if (findStr.equals(u.getName())) return true;
        return false;
    }

    private boolean checkEvent(String findStr) {
        for (Event u : this.events) if (findStr.equals(u.getName())) return true;
        return false;
    }

    private Event getEventByString(String name) {
        for (Event u : this.events)
            if (u.getName().equals(name))
                return u;

        return null;
    }

    public Event getEvent(String name) {
        return this.getEventByString(name);
    }

    private User getUserByString(String name) {
        for (User u : this.users)
            if (u.getName().equals(name))
                return u;

        return null;
    }

    public User getUser(String name) {
        return this.getUserByString(name);
    }

    private boolean checkAvailableTime(User userName, Event eventName) {
        ArrayList<Event> userEvents = userName.getEvents();

        for (Event ev : userEvents) {
            if (ev.overlapsWith(eventName))
                return false;
        }

        return true;
    }
}
