package lab9.event;

import java.util.Date;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


/**
 * A class representing an event and its properties
 */
public class Event {
    /**
     * Name of event
     */
    private String name;
    private Date startTime;
    private Date endTime;
    private String costPerHr;
    private long durationHours;
    private BigInteger cost;

    public Event(String name, Date startTime, Date endTime, String costPerHr) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.costPerHr = costPerHr;
        this.durationHours = (long) (endTime.getTime() - startTime.getTime()) / (1000 * 60 * 60);
        this.cost = new BigInteger(costPerHr);
        this.cost = this.cost.multiply(BigInteger.valueOf(this.durationHours));
    }

    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }


    public String toString() {
        String temp;
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");

        String strStartDate = df.format(this.startTime);
        String strEndDate = df.format(this.endTime);
        String thisCost = this.cost.toString();

        temp = "%s\n" +
                "Waktu mulai: %s\n" +
                "Waktu selesai: %s\n" +
                "Biaya kehadiran: %s\n";

        return String.format(temp, this.name, strStartDate, strEndDate, thisCost);
    }

    public BigInteger getCost() {
        return this.cost;
    }

    public boolean overlapsWith(Event other) {
        if (((this.startTime.before(other.getStartTime())) && (this.endTime.after(other.getStartTime()))) ||
                ((this.startTime.before(other.getEndTime())) && (this.endTime.after(other.getEndTime())))) {
            return false;
        }

        return true;
    }
}