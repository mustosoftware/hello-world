package ticket;

import movie.Movie;

public class Ticket implements Cloneable {

    private static final int standardFee = 60000;
    private int harga;
    private Movie film;
    private String hari;
    private boolean triD;

    public Ticket(Movie film, String hari, boolean triD) {
        this.film = film;
        this.hari = hari;
        this.triD = triD;
        
        this.harga = Ticket.standardFee;
        
        if ((hari.equals("Sabtu")) || (hari.equals("Minggu"))) {
            this.harga += 40000;
        }
        
        if (triD)
            this.harga = (int) ((1.2f) * this.harga);
    }

    public int getHarga() {
        return this.harga;
    }

    public Movie getMovie() {
        return this.film;
    }

    public String getHari() {
        return this.hari;
    }

    public boolean isTriD() {
        return this.triD;
    }

    public String info() {
        String information = "------------------------------------------------------------------ \n"
                + "Film            : " + this.film.getJudul() + "\n"
                + "Jadwal Tayang   : " + this.hari + "\n"
                + "Jenis           : " + this.triD + "\n"
                + "------------------------------------------------------------------";

        return information;
    }

}
