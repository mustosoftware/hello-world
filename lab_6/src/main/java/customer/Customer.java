package customer;

import theater.Theater;
import ticket.Ticket;
import movie.Movie;

public class Customer {

    private String nama;
    private int umur;
    private boolean jenisKelamin;

    public Customer(String nama, boolean jenisKelamin, int umur) {
        this.nama = nama;
        this.umur = umur;
        this.jenisKelamin = jenisKelamin;
    }

    public Ticket orderTicket(Theater bioskop, String judul, String hari, String jenis) {
        Ticket ticket;
        if (bioskop.checkTicket(judul, hari, jenis)) {
            ticket = bioskop.takeTicket(judul, hari, jenis, false);
            if ((ticket.getMovie().getRating().equals("Remaja")) && (this.umur<13))
                System.out.println(this.nama + " masih belum cukup umur untuk menonton " + judul + " dengan rating " + ticket.getMovie().getRating());
            else if ((ticket.getMovie().getRating().equals("Dewasa")) && (this.umur<17))
                System.out.println(this.nama + " masih belum cukup umur untuk menonton " + judul + " dengan rating " + ticket.getMovie().getRating());
            else
            {
               ticket = bioskop.takeTicket(judul, hari, jenis, true);
               System.out.println(nama + " telah membeli tiket " + judul + " jenis " + jenis + " di " + bioskop.getNama() + " pada hari " + hari + " seharga Rp. " + ticket.getHarga());
            }
        } else {
            System.out.println("Tiket untuk film " + judul + " jenis " + jenis + " dengan jadwal " + hari + " tidak tersedia di " + bioskop.getNama());

            ticket = null;
        }

        return ticket;
    }

    public void findMovie(Theater bioskop, String judul) {
        Movie[] movies = bioskop.getMovies();
        Movie Mov = null;
        boolean available = false;

        for (Movie M : movies) {
            if (M.getJudul().equals(judul)) {
                available = true;
                Mov = M;
                break;
            }
        }

        if (available) {
            System.out.println(Mov.info());
        } else {
            System.out.println("Film " + judul + " yang dicari " + this.nama + " tidak ada di bioskop " + bioskop.getNama());
        }
    }

}
