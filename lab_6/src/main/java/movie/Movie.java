package movie;

public class Movie {

    private String judul;
    private String rating;
    private int durasi;
    private String genre;
    private String jenis;

    public Movie(String judul, String rating, int durasi, String genre, String jenis) {
        this.judul = judul;
        this.rating = rating;
        this.durasi = durasi;
        this.genre = genre;
        this.jenis = jenis;
    }

    public String getJudul() {
        return this.judul;
    }

    public String getRating() {
        return this.rating;
    }

    public int getDurasi() {
        return this.durasi;
    }

    public String getGenre() {
        return this.genre;
    }

    public String getJenis() {
        return this.jenis;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Movie) && (((Movie) o).getJudul().equals(this.judul));
    }

    /**
     * Get information about movie
     *
     * @return Information about the movie
     */
    public String info() {
        String information = "------------------------------------------------------------------ \n"
                + "Judul   : " + this.judul + "\n"
                + "Genre   : " + this.genre + "\n"
                + "Durasi  : " + this.durasi + " menit\n"
                + "Rating  : " + this.rating + "\n"
                + "Jenis   : Film " + this.jenis + "\n"
                + "------------------------------------------------------------------";

        return information;
    }

}
