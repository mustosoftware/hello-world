package theater;

import java.util.ArrayList;
//import java.util.Arrays;
import movie.Movie;
import ticket.Ticket;

public class Theater {

    //private static ArrayList<Theater> theaters;
    private int saldo;
    private String nama;
    private Movie[] movies;
    private ArrayList<Ticket> tickets;

    public Theater(String nama, int saldo, ArrayList<Ticket> tickets, Movie[] movies) {
        this.nama = nama;
        this.saldo = saldo;
        this.movies = movies;
        this.tickets = tickets;

        //Theater.theaters.add(this);
    }

    public boolean checkTicket(String judul, String hari, String jenis) {
        boolean available = false;
        boolean isTriD = (jenis.equals("3 Dimensi"));

        for (Ticket T : tickets) {
            if ((T.getMovie().getJudul().equals(judul)) && (T.getHari().equals(hari)) && (T.isTriD() == isTriD)) {
                available = true;
                break;
            }
        }

        return available;
    }

    public Ticket takeTicket(String judul, String hari, String jenis, boolean taken) {
        int idx = -1;
        boolean available = false;
        Ticket temp;
        boolean isTriD = (jenis.equals("3 Dimensi"));

        for (int i = 0; i < tickets.size(); i++) {
            if ((tickets.get(i).getMovie().getJudul().equals(judul)) && (tickets.get(i).getHari().equals(hari)) && (tickets.get(i).isTriD() == isTriD)) {
                available = true;
                idx = i;
                break;
            }
        }

        if (available) {
            temp = tickets.get(idx);
            if (taken){
                this.saldo += tickets.get(idx).getHarga();
                tickets.remove(idx);
            }
            return temp;
        } else {
            return null;
        }
    }

    public Movie[] getMovies() {
        return this.movies;
    }

    public String getNama() {
        return this.nama;
    }

    public int getSaldo() {
        return this.saldo;
    }

    public static String infoTheaters(Theater[] theaters) {
        int totalSaldo = 0;

        for (Theater T : theaters) {
            totalSaldo += T.getSaldo();
        }

        String infos = "Total uang yang dimiliki Koh Mas : Rp. " + totalSaldo + "\n"
                + "------------------------------------------------------------------\n";

        for (int i = 0; i < theaters.length; i++) {
            if (i == theaters.length - 1) {
                infos += "Bioskop     : " + theaters[i].getNama()
                        + "\nSaldo Kas   : Rp. " + theaters[i].getSaldo() + "\n";
            } else {
                infos += "Bioskop     : " + theaters[i].getNama()
                        + "\nSaldo Kas   : Rp. " + theaters[i].getSaldo() + "\n\n";
            }
        }

        infos += "------------------------------------------------------------------";

        return infos;
    }

    public static void printTotalRevenueEarned(Theater[] theaters) {
        System.out.println(infoTheaters(theaters));
    }

    public String info() {
        String daftarFilm = "";

        for (int i = 0; i < movies.length; i++) {
            if (i == movies.length - 1) {
                daftarFilm += movies[i].getJudul();
            } else {
                daftarFilm += movies[i].getJudul() + ", ";
            }
        }

        String information = "------------------------------------------------------------------ \n"
                + "Bioskop                 : " + this.nama + "\n"
                + "Saldo Kas               : " + this.saldo + "\n"
                + "Jumlah tiket tersedia   : " + this.tickets.size() + "\n"
                + "Daftar Film tersedia    : " + daftarFilm + "\n"
                + "------------------------------------------------------------------";

        return information;
    }

    public void printInfo() {
        System.out.println(this.info());
    }

}
