import java.util.Scanner;

//not yet done
public class Bingo {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        Number[][] n = new Number[5][5];
        for (int i = 0; i<5; i++)
            for (int j = 0; j<5; j++)
            {
                int val = Integer.parseInt(input.next());
                n[i][j] = new Number(val, i, j);
            }
        
        BingoCard bingo1 = new BingoCard(n);
        boolean firstRun = true;
        
        //System.out.println("Done input");
        input.nextLine();   //clear the buffer from stdin
        
        while (!(bingo1.isBingo()))
        {
            String[] command = input.nextLine().split(" ");
            
            if (command[0].equals("MARK"))
            {
                int num = Integer.parseInt(command[1]);
                System.out.println(bingo1.markNum(num));
                if (bingo1.isBingo())
                {
                    System.out.println("BINGO!");
                    System.out.println(bingo1.info());
                }
            }
            else if (command[0].equals("INFO"))
            {
                System.out.println(bingo1.info());
            }
            else if (command[0].equals("RESTART"))
            {
                bingo1.restart();
            }
            else
            {
                System.out.println("Incorrect command");
            }
        }
    }
}