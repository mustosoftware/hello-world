/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {
	private String name;
    private Number[][] numbers;
    private Number[] numberStates;
    private boolean isBingo;

    public BingoCard(Number[][] numbers) {
        this.numbers = numbers;
        //this.numberStates = numberStates;
        this.isBingo = false;
		this.name = "bingo";
    }
	
	public BingoCard(Number[][] numbers, String name) {
		this.numbers = numbers;
		this.isBingo = false;
		this.name = name;
	}
    
    /**
     * Check BingoCard whether bingo or not. It starts to check from certain point
     * @param x X coordinate point
     * @param y Y coordinate point
     * @return True if bingo!
     */
    private boolean checkBingo(int x, int y)
    {
        int maxX = this.numbers.length;
        int maxY = this.numbers[0].length;
        
        return (checkBingoV(x, maxY) || checkBingoH(y, maxX) || checkBingoD());
    }
    
    /**
     * Check bingo vertically to the x
     * @param x X point
     * @param maxY Max y
     * @return True if bingo! vertical to the X
     */
    private boolean checkBingoV(int x, int maxY)
    {
        boolean bingo = true;
        
        for (int y = 0; y < maxY; y++)
            if (!(this.numbers[x][y].isChecked()))
            {
                bingo = false;
                break;
            }
        
        return bingo;
    }
    
    /**
     * Check bingo horizontally to the y
     * @param y Y point
     * @param maxX Max x
     * @return True if bingo! horizontal to the Y
     */
    private boolean checkBingoH(int y, int maxX)
    {
        boolean bingo = true;
        
        for (int x = 0; x < maxX; x++)
        {
            if (!(this.numbers[x][y].isChecked()))
            {
                bingo = false;
                break;
            }
        }
        
        return bingo;
    }
    
    private boolean checkBingoD()
    {
        boolean bingo = true;
        
        for (int i = 0; i < 5; i++)
        {
            if (!(this.numbers[i][i].isChecked()))
            {
                bingo = false;
                break;
            }
        }
        
        if (!bingo)
        {
            bingo = true;
            for (int i = 0; i < 5; i++)
            {
                if (!(this.numbers[i][4-i].isChecked()))
                {
                    bingo = false;
                    break;
                }
            }
        }
        
        return bingo;
    }

    public Number[][] getNumbers() {
        return numbers;
    }

    public void setNumbers(Number[][] numbers) {
        this.numbers = numbers;
    }

    public Number[] getNumberStates() {
        return numberStates;
    }

    public void setNumberStates(Number[] numberStates) {
        this.numberStates = numberStates;
    }	

    public boolean isBingo() {
        return isBingo;
    }

    public void setBingo(boolean isBingo) {
        this.isBingo = isBingo;
    }

    public String markNum(int num){
        boolean found = false;
        String ret = "";
        int posX, posY; posX = posY = 0;

        //iterator to first-dimention index
        int k = 0;
        for (Number[] i : numbers) {
            //iterator to second-dimention index
            int l = 0;
            for (Number j : i)
            {
                if (this.numbers[k][l].getValue() == num)
                    if (this.numbers[k][l].isChecked())
                    {
                        ret = num + " sebelumnya sudah tersilang";
                        found = true;
                        posX = k;
                        posY = l;
                        break;
                    }
                    else
                    {
                        this.numbers[k][l].setChecked(true);
                        ret = num + " tersilang";
                        found = true;
                        posX = k;
                        posY = l;
                        break;
                    }
                l++;
            }
            if (found) break;
            k++;
        }

        if (!found)
        {
            return "Kartu tidak memiliki angka " + num;
        }
        
        if (checkBingo(posX, posY))
            this.setBingo(true);
        
        return ret;
    }

    public String info(){
        String temp = "";
        
        int k = 0;
        //iterator to first-dimention index
        for (Number[] i : numbers) {
            //iterator to second-dimention index
            int l = 0;
            temp += "|";
            
            for (Number j : i)
            {
                if (this.numbers[k][l].isChecked())
                    temp += " X  |";
                else
                    temp += " " + this.numbers[k][l].getValue() + " |";
                l++;
            }
            temp += ( k!=4 ) ? "\n" : "";
            k++;
        }
        
        return temp;
    }

    public void restart(){
        int k = 0;
        //iterator to first-dimention index
        for (Number[] i : numbers) {
            //iterator to second-dimention index
            int l = 0;
            for (Number j : i)
            {
                this.numbers[k][l].setChecked(false);
                l++;
            }
            k++;
        }
        
        System.out.println("Mulligan!");
    }
}