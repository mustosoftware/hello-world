import java.util.Scanner;

/**
* @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
* Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
* Anda sangat disarankan untuk menggunakan template ini.
* Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
* selama tidak bertentangan dengan ketentuan soal.
*
* Cara penggunaan template ini adalah:
* 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
* 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
*
* Code Author (Mahasiswa):
* @author Ahmad Mustofa, NPM 1706043512, Kelas DDP2 E, GitLab Account: mustosoftware
*/

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);
		
		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
						"--------------------\n" +
						"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		short panjang = Short.parseShort(input.nextLine());
		if (!(panjang>0 && panjang<=250))
		{
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!\n");
			return;
		}
		System.out.print("Lebar Tubuh (cm)       : ");
		short lebar = Short.parseShort(input.nextLine());
		if (!(lebar>0 && lebar<=250))
		{
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!\n");
			return;
		}
		System.out.print("Tinggi Tubuh (cm)      : ");
		short tinggi = Short.parseShort(input.nextLine());
		if (!(tinggi>0 && tinggi<=250))
		{
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!\n");
			return;
		}
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = Float.parseFloat(input.nextLine());
		if (!(berat>0 && berat<=150))
		{
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!\n");
			return;
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		short makanan = (short) Integer.parseInt(input.nextLine());
		if (!(makanan>0 && makanan<=20))
		{
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!\n");
			return;
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		short jumlahCetakan = (short) Integer.parseInt(input.nextLine());


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		
		short rasio = ((short)((68.5)/((36/100.0) * (189/100.0) * (71/100.0) ) ));

		for (int i = 0; i<jumlahCetakan; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + (i+1) + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if ("".equals(catatan)) catatan = "Tidak ada catatan tambahan";
			else catatan = "Catatan: " + catatan;

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK " + penerima +
							"\n--------------------\n" +
							nama + " - " + alamat + "\n" +
							"Lahir pada tanggal " + tanggalLahir + "\n" +
							"Rasio Berat Per Volume\t= " + rasio + " kg/m^3\n" +
							catatan + "\n\n";
			System.out.println(hasil);
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)



		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		int jumlahASCIINama = 0;
		for (int i = 0; i<nama.length(); i++) jumlahASCIINama += (int)nama.charAt(i);
		String nomorKeluarga = nama.substring(0,1)+ ((panjang*tinggi*lebar) + jumlahASCIINama) % 10000;

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (50000) * 365 * makanan;

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		short tahunLahir = Short.parseShort(tanggalLahir.split("-")[2]); // lihat hint jika bingung
		short umur = (short) (2018 - (tahunLahir));

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String namaApartment = "";
		String kabupaten = "";

		if (umur>=19 && umur<=1018)
		{
			if (anggaran>=0 && anggaran<=100000000)
			{
				namaApartment = "Teksas";
				kabupaten = "Sastra";
			}
			else if (anggaran>100000000)
			{
				namaApartment = "Mares";
				kabupaten = "Margonda";
			}
		}
		else if (umur>=0 && umur<=18)
		{
			namaApartment = "PPMT";
			kabupaten = "Margonda";
		}


		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "REKOMENDASI APARTEMEN\n" +
							"--------------------\n" +
							"MENGETAHUI: Identitas keluarga: " + " - " + nomorKeluarga + "\n" +
							"MENIMBANG:  Anggaran makanan tahunan: Rp " + anggaran + "\n" +
							"            Umur kepala keluarga: " + umur + " tahun\n" +
							"MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" +
							namaApartment + ", kabupaten " + kabupaten + "\n" ;

		System.out.println(rekomendasi);
		input.close();
	}
}