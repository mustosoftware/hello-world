import java.util.Scanner;

/**
 * Solusi Soal Tutorial dan Soal Bonus
 *
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Ichlasul Affan (Solusi), NPM 1606895606, Kelas null, GitLab Account: ichlaffterlalu
 * @author Arga Ghulam Ahmad (Solusi), NPM 1606821601, Kelas null, Gitlab Account: argaghulamahmad
 */

public class SistemSensus {
    public static void main(String[] args) {
        // Buat input scanner baru
        Scanner input = new Scanner(System.in);


        // TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        // User Interface untuk meminta masukan
        System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
                "--------------------\n" +
                "Nama Kepala Keluarga   : ");
        String nama = input.nextLine();
        System.out.print("Alamat Rumah           : ");
        String alamat = input.nextLine();
        System.out.print("Panjang Tubuh (cm)     : ");
        short panjang = Short.parseShort(input.nextLine());
        // TODO Validasi input
        if (panjang <= 0 || panjang > 250) {
            // TODO Tampilkan pesan error lalu hentikan program
            System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
            return; // keluar dari method main untuk menghentikan program
        }
        System.out.print("Lebar Tubuh (cm)       : ");

        short lebar = Short.parseShort(input.nextLine());
        // TODO Validasi input
        if (lebar <= 0 || lebar > 250) {
            // TODO Tampilkan pesan error lalu hentikan program
            System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
            return; // keluar dari method main untuk menghentikan program
        }
        System.out.print("Tinggi Tubuh (cm)      : ");

        short tinggi = Short.parseShort(input.nextLine());
        // TODO Validasi input
        if (tinggi <= 0 || tinggi > 250) {
            // TODO Tampilkan pesan error lalu hentikan program
            System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
            return; // keluar dari method main untuk menghentikan program
        }
        System.out.print("Berat Tubuh (kg)       : ");

        float berat = Float.parseFloat(input.nextLine());
        // TODO Validasi input
        if (berat <= 0 || berat > 150) {
            // TODO Tampilkan pesan error lalu hentikan program
            System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
            return; // keluar dari method main untuk menghentikan program
        }
        System.out.print("Jumlah Anggota Keluarga: ");

        byte jumlahAnggota = Byte.parseByte(input.nextLine());
        // TODO Validasi input
        if (jumlahAnggota <= 0 || jumlahAnggota > 20) {
            // TODO Tampilkan pesan error lalu hentikan program
            System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
            return; // keluar dari method main untuk menghentikan program
        }

        System.out.print("Tanggal Lahir          : ");
        String tanggalLahir = input.nextLine();
        System.out.print("Catatan Tambahan       : ");
        String catatan = input.nextLine();
        System.out.print("Jumlah Cetakan Data    : ");
        byte jumlahCetakan = Byte.parseByte(input.nextLine());

        // TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        int rasio = (int) (berat / (((float) panjang / 100) * ((float) lebar / 100) * ((float) tinggi / 100)));

        for (byte i = 1; i <= jumlahCetakan; i++) {
            // TODO Minta masukan terkait nama penerima hasil cetak data
            System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
            String penerima = input.nextLine().toUpperCase();  // Lakukan baca input lalu langsung jadikan uppercase

            // TODO Periksa ada catatan atau tidak
            if (catatan.isEmpty()) catatan = "Tidak ada catatan tambahan";
            else catatan = "Catatan: " + catatan;

            // TODO Cetak data (ganti string kosong agar keluaran sesuai)
            String hasil = "DATA SIAP DICETAK UNTUK " + penerima.toUpperCase() + "\n" +
                    "--------------------\n" +
                    nama + " - " + alamat + "\n" +
                    "Lahir pada tanggal " + tanggalLahir + "\n" +
                    "Rasio Berat Per Volume     = " + rasio + " kg/m^3\n" +
                    catatan + "\n";
            System.out.println(hasil);
        }


        // TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
        // TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
        int angka = (int) (panjang * lebar * tinggi);
        for (byte i = 0; i < nama.length(); i++) {
            angka += (int) nama.charAt(i);
        }
        angka = angka % 10000;

        // TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
        String nomorKeluarga = nama.charAt(0)+""+angka;

        // TODO Hitung anggaran makanan per tahun (rumus lihat soal)
        int anggaran (int) (50000 * 365 * jumlahAnggota);

        // TODO Hitung umur dari tanggalLahir (rumus lihat soal)
        short tahunLahir = Short.parseShort(tanggalLahir.split("-")[2]); // lihat hint jika bingung
        short umur = (short) (2018 - tahunLahir);

        // TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
        String tempat = "";
        String kabupaten = "";

        if (0 <= umur && umur <= 18) {
            tempat = "PPMT";
            kabupaten = "Rotunda";
        }
        else if (18 < umur && umur < 1018) {
            if (0 < anggaran && anggaran <= 100000000) {
                tempat = "Teksas";
                kabupaten = "Sastra";
            }
            else if (100000000 < anggaran) {
                tempat = "Mares";
                kabupaten = "Margonda";
            }
        }

        // TODO Kembalikan rekomendasi yang ingin dicetak (ganti string kosong agar keluaran sesuai)
        String rekomendasi = "REKOMENDASI TEMPAT KONSERVASI\n" +
                "--------------------\n" +
                "MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga + "\n" +
                "MENIMBANG:  Anggaran makanan tahunan: Rp " + anggaran + "\n" +
                "            Umur kepala keluarga: " + umur + " tahun\n" +
                "MEMUTUSKAN: keluarga akan ditempatkan di: " + "\n" +
                tempat + ", kabupaten " + kabupaten;
        System.out.println(rekomendasi);

        input.close();
    }
}