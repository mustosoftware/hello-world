
import java.util.ArrayList;

class Korporasi {

    public int MAX_STAFF_SALARY;
    private int maxKaryawan;

    private ArrayList<Karyawan> karyawans = new ArrayList();

    public Korporasi(int maxKaryawan, int maxStaffSalary) {
        this.maxKaryawan = maxKaryawan;
        MAX_STAFF_SALARY = maxStaffSalary;
    }

    public Karyawan findKaryawan(String nama) {
        for (Karyawan k : karyawans) {
            if (nama.toLowerCase().equals(k.getName().toLowerCase())) {
                return k;
            }
        }

        return null;
    }

    public String addKaryawan(String name, String tipe, int gajiAwal) {
        Karyawan newKaryawan;

        if (tipe.equals("MANAGER")) {
            newKaryawan = new Manager(name, gajiAwal);
        } else if (tipe.equals("STAFF")) {
            newKaryawan = new Staff(name, gajiAwal);
        } else {
            newKaryawan = new Intern(name, gajiAwal);
        }

        this.karyawans.add(newKaryawan);
        return String.format("%s mulai bekerja sebagai %s di PT. NAMPAN", name, tipe);
    }

    public String addBawahan(String karyawan, String bawahan) {
        Karyawan staf1 = findKaryawan(karyawan);
        Karyawan staf2 = findKaryawan(bawahan);
        String strBawahan = "Karyawan %s berhasil ditambahkan menjadi bawahan %s";

        if ((staf1 == null) || (staf2 == null)) {
            return "Nama tidak berhasil ditemukan";
        } else {
            if (staf1.isBawahan(staf2)) {
                String temp = "Karyawan %s telah menjadi bawahan %s";
                return String.format(temp, bawahan, karyawan);
            } else {
                if (staf1.tipe().equals("Manager")) {
                    if ((staf2.tipe().equals("Staff") || staf2 instanceof Intern) && staf1.getJumlahBawahan() < 10) {
                        staf1.addBawahan(staf2);
                        return String.format(strBawahan, bawahan, karyawan);
                    } else {
                        return "Anda tidak layak memiliki bawahan";
                    }
                } else if (staf1.tipe().equals("Staff")) {
                    if (staf2 instanceof Intern && staf1.getJumlahBawahan() < 10) {
                        staf1.addBawahan(staf2);
                        return String.format(strBawahan, bawahan, karyawan);
                    } else {
                        return "Anda tidak layak memiliki bawahan";
                    }

                } else if (staf1.tipe().equals("Intern")) {
                    return "Anda tidak layak memiliki bawahan";
                }

                return "";
            }
        }
    }

    public String status(String karyawan) {
        Karyawan k = findKaryawan(karyawan);

        if (k == null) {
            return "Karyawan tidak ditemukan";
        } else {
            return karyawan + " " + k.getGaji();
        }
    }

    public void gajian() {
        System.out.println("Semua karyawan telah diberikan gaji");

        for (Karyawan k : karyawans) {
            k.gajian();
            if (k.countGajian() == 6) {
                k.naikGaji();
                if (k.getGaji() >= MAX_STAFF_SALARY && k.tipe().equals("Staff")) {
                    k.naikPangkat();
                }
            }
        }

    }
}
