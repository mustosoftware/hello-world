
import java.util.ArrayList;

class Karyawan {

    protected ArrayList<Karyawan> bawahan = new ArrayList();
    protected int gaji;
    protected int nGajian;
    protected String name;
    protected String tipe;

    public Karyawan(String name, int gaji) {
        this.name = name;
        this.gaji = gaji;
    }

    public void addBawahan(Karyawan karyawan) {
        this.bawahan.add(karyawan);
    }

    public void gajian() {
        this.nGajian++;
    }

    public int countGajian() {
        return this.nGajian;
    }

    public void naikGaji() {
        int oldGaji = this.gaji;
        int newGaji = (int) 1.1 * this.gaji;

        System.out.println(String.format("%s mengalami kenaikan gaji sebesar 10%% dari %d menjadi %d", this.name, oldGaji, newGaji));
        this.gaji = newGaji;
    }

    public void naikPangkat() {
        if (this.tipe.equals("Staff")) {
            this.tipe = "Manager";
        }

        System.out.println(String.format("Selamat, %s telah dipromosikan menjadi MANAGER", this.name));
    }

    public int getJumlahBawahan() {
        return this.bawahan.size();
    }

    public String getName() {
        return this.name;
    }

    public int getGaji() {
        return this.gaji;
    }

    public boolean isBawahan(Karyawan b) {
        for (Karyawan k : bawahan) {
            if (k == b) {
                return true;
            }
        }

        return false;
    }

    public String tipe() {
        return this.tipe;
    }
}
