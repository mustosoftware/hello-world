/**
 *
 * @author mustosoft
 */
public class Manusia {
    private static Manusia[] human = new Manusia[20];
    private static int lenHuman = 0;
    private boolean alive;
    private String nama;
    private int umur, uang;
    private float kebahagiaan;
    
    public Manusia(String nama, int umur)
    {
        this.alive = true;
        this.nama =  nama;
        this.uang = 50000;
        this.umur = umur;
        this.kebahagiaan = 50;
        
        human[lenHuman] = this;
        lenHuman += 1;
    }
    
    public Manusia(String nama, int umur, int uang)
    {
        this.alive = true;
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = 50;
        
        human[lenHuman] = this;
        lenHuman += 1;
    }
    
    /**
     * get life state of Manusia
     * @return True if this still alive
     */
    public boolean isAlive()
    {
        return alive;
    }
    
    public void soutDead()
    {
        if (!this.isAlive())
            System.out.println(this.nama + " telah tiada");
    }
    
    public void meninggal()
    {
        if (this.alive)
        {
            alive = false;
            System.out.println(this.nama + " meninggal dengan tenang, kebahagiaan : " + this.kebahagiaan);
            this.nama = "Almarhum " + this.nama;
            
            int iHuman = Manusia.lenHuman - 1;
            Manusia lastHuman = human[iHuman];
            
            if (this == lastHuman)
            {
                System.out.println("Semua harta " + this.nama + " hangus");
            }
            else
            {
                while (iHuman>0)
                {
                    if (lastHuman.isAlive())
                    {
                        lastHuman.setUang(lastHuman.getUang() + this.uang);
                        this.setUang(0);
                        break;
                    }
                    iHuman--;
                    lastHuman = human[iHuman];
                }

                lastHuman.setUang(lastHuman.getUang() + this.uang);
                this.setUang(0);

                System.out.println("Semua harta " + this.nama + " disumbangkan untuk " + lastHuman.getNama());
            }
        }
        else
            System.out.println(this.nama + " sudah meninggal dengan tenang, kebahagiaan : " + this.kebahagiaan);
    }
    
    public String toString()
    {
        String temp =   "Nama\t: " + this.nama + "\n" +
                        "Umur\t: " + this.umur + "\n" +
                        "Uang\t: " + this.uang + "\n" +
                        "Kebahagiaan\t: " + this.kebahagiaan;
        return temp;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    }
    
    /* getter and setter */
    
    public String getNama()
    {
        return this.nama;
    }
    
    public int getUmur()
    {
        return this.umur;
    }
    
    /**
     * Get amount uang 
     * @return int uang
     */
    public int getUang()
    {
        return this.uang;
    }
    
    /**
     * Set amount of uang
     * @param nominal   amount of uang 
     * @return amount of uang after set
     */
    public boolean setUang(int nominal)
    {
        if (this.alive)
        {
            this.uang = nominal;
            return true;
        }
        
        System.out.println(this.nama + " telah tiada");
        return false;
    }
    
    /**
     * Get kebahagiaan
     * @return kebahagiaan (float)
     */
    public float getKebahagiaan()
    {
        return this.kebahagiaan;
    }
    
    /**
     * Set kebahagiaan by float kebahagiaan
     * @param kebahagiaan float
     * @return kebahagiaan after set
     */
    public boolean setKebahagiaan(float kebahagiaan)
    {
        if (this.alive)
        {
            if (kebahagiaan>100)
                this.kebahagiaan = 100f;
            else if (kebahagiaan<0)
                this.kebahagiaan = 0f;
            else
                this.kebahagiaan = kebahagiaan;

            return true;
        }
        soutDead();
        return false;
    }
    
    /** End of setter and getter **/
    
    /**
     * Beri uang kepada [penerima]
     * @param penerima Manusia penerima uang
     */
    public void beriUang(Manusia penerima)
    {
        if (this.isAlive())
        {
            String s_nama = penerima.nama;
            int jumlah = 0;

            for (int i = 0; i<s_nama.length(); i++)
                jumlah += (int) s_nama.charAt(i);
            jumlah *= 100;

            if (jumlah>this.uang)
                System.out.println(this.nama + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
            else
            {
                penerima.setUang(penerima.getUang() + jumlah);
                this.uang -= jumlah;

                float kebahagiaanTambahan = (float) jumlah/6000;
                this.setKebahagiaan(this.kebahagiaan + kebahagiaanTambahan);
                penerima.setKebahagiaan(penerima.getKebahagiaan() + kebahagiaanTambahan);

                System.out.println(this.nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            }
            
        }
        else
            soutDead();
            
    }
    
    /**
     * Beri uang ke penerima sejumlah jumlah
     * @param penerima Manusia penerima
     * @param jumlah Jumlah uang
     */
    public void beriUang(Manusia penerima, int jumlah)
    {
        if (this.isAlive())
        {
            if (this.uang<jumlah)
                System.out.println(this.nama + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
            else
            {
                float kebahagiaanTambahan = jumlah / 6000f;
                this.uang -= jumlah;
                penerima.setUang(penerima.getUang() + jumlah);

                this.setKebahagiaan(this.kebahagiaan + kebahagiaanTambahan);
                penerima.setKebahagiaan(penerima.getKebahagiaan() + kebahagiaanTambahan);

                System.out.println(this.nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            }       
        }
        else
            soutDead();
    }
    
    /**
     * Method bekerja
     * @param durasi Durasi kerja
     * @param bebanKerja Beban kerja per durasi
     */
    public void bekerja(int durasi, int bebanKerja)
    {
        if (this.isAlive())
        {
            if (this.umur < 18)
                System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
            else
            {
                int bebanKerjaTotal = durasi * bebanKerja;

                if (bebanKerjaTotal <= this.kebahagiaan)
                {
                    int pendapatan = bebanKerjaTotal * 10000;

                    this.setKebahagiaan(this.kebahagiaan - bebanKerjaTotal);
                    this.uang += pendapatan;

                    System.out.println(this.nama + " bekerja full time, total pendapatan : " + pendapatan);
                }
                else 
                {
                    int durasiBaru = (int) (this.kebahagiaan/bebanKerja);
                    bebanKerjaTotal = bebanKerja * durasiBaru;
                    int pendapatan = bebanKerjaTotal * 10000;

                    this.setKebahagiaan(this.kebahagiaan - bebanKerjaTotal);
                    this.uang += pendapatan;

                    System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
                }
            }
        }
        else
            soutDead();
    }
    
    public void rekreasi(String namaTempat)
    {
        if (this.isAlive())
        {
            int biaya = namaTempat.length() * 10000;

            if (this.uang >= biaya)
            {
                float kebahagiaanBaru = this.kebahagiaan + namaTempat.length();

                this.uang -= biaya;
                this.setKebahagiaan(kebahagiaanBaru);

                System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang :)");
            }
            else 
                System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
        }
        else
            soutDead();
    }
    
    public void sakit(String namaPenyakit)
    {
        if (this.isAlive())
        {
            float kebahagiaanBaru = this.kebahagiaan - namaPenyakit.length();

            this.setKebahagiaan(kebahagiaanBaru);

            System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :O");
        }
        else
            soutDead();
    }
    
}
