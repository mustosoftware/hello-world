public class TestCase{
	public static void main(String x[])
	{
		Manusia manusia_1 = new Manusia("Calon Ahli Kubur", 20);
		Manusia manusia_2 = new Manusia("Tobat Sebelum Terlambat", 40, 200000);
		Manusia manusia_3 = new Manusia("Calon Suami Idaman", 23);
		Manusia manusia_4 = new Manusia("Fakir Miskin", 25, 10000);
		manusia_2.beriUang(manusia_1, 100000);
		manusia_1.beriUang(manusia_4, 2000);
		manusia_1.sakit("Kikir");
		manusia_1.beriUang(manusia_4, 80000);
		manusia_1.meninggal();
		System.out.println(manusia_1);
		System.out.println(manusia_4);
		manusia_1.sakit("Sakitan");
		manusia_1.rekreasi("Kuburan sebelah");
		manusia_1.bekerja(24, 10);
		manusia_1.beriUang(manusia_4);
		manusia_1.meninggal();
		manusia_3.beriUang(manusia_1);
		System.out.println(manusia_1);
		System.out.println(manusia_4);
	}
}


