<<<<<<< HEAD

import character.*;
import java.util.ArrayList;

public class Game {

    ArrayList<Player> player = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     *
     * @param name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila
     * tidak ditemukan
     */
    public Player find(String name) {
//        for (Player p : player) {
//            if ((p != null) && p.getName().equals(name) ) {
//                return p;
//            }
//        }

        for (int i = 0; i < player.size(); i++) {
            if ((player.get(i) != null) && player.get(i).getName().equals(name)) {
                return player.get(i);
            }
        }

=======
import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
>>>>>>> 41d825197b6b2c762feddfa80b1fd1ffa05197f6
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
<<<<<<< HEAD
     *
     * @param chara nama karakter yang ingin ditambahkan
     * @param tipe tipe dari karakter yang ingin ditambahkan terdiri dari
     * monster, magician dan human
     * @param hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh
     * "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        if (tipe.equals("Human")) {
            if (find(chara) == null) {
                Human human = new Human(chara, hp);
                player.add(human);

                return chara + " ditambah ke game";
            } else {
                return "Sudah ada karakter bernama " + chara;
            }
        } else if (tipe.equals("Monster")) {
            if (find(chara) == null) {
                Monster monster = new Monster(chara, hp);
                player.add(monster);

                return chara + " ditambah ke game";
            } else {
                return "Sudah ada karakter bernama " + chara;
            }
        } else if (tipe.equals("Magician")) {
            if (find(chara) == null) {
                Magician magician = new Magician(chara, hp);
                player.add(magician);

                return chara + " ditambah ke game";
            } else {
                return "Sudah ada karakter bernama " + chara;
            }
        }
=======
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
>>>>>>> 41d825197b6b2c762feddfa80b1fd1ffa05197f6
        return "";
    }

    /**
<<<<<<< HEAD
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar
     * hanya bisa dilakukan oleh monster
     *
     * @param chara nama karakter yang ingin ditambahkan
     * @param tipe tipe dari karakter yang ingin ditambahkan terdiri dari
     * monster, magician dan human
     * @param hp hp dari karakter yang ingin ditambahkan
     * @param roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh
     * "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        if (tipe.equals("Monster")) {
            if (find(chara) == null) {
                Monster monster = new Monster(chara, hp);
                player.add(monster);

                return chara + " ditambah ke game";
            } else {
                return "Sudah ada karakter bernama " + chara;
            }
        }

=======
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
>>>>>>> 41d825197b6b2c762feddfa80b1fd1ffa05197f6
        return "";
    }

    /**
     * fungsi untuk menghapus character dari game
<<<<<<< HEAD
     *
     * @param chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        Player p = find(chara);
        if (p != null) {
            player.remove(p);
            return chara + " dihapus dari game";
        } else {
            return "Tidak ada " + chara;
        }
    }

    /**
     * fungsi untuk menampilkan status character dari game
     *
     * @param chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        Player p = find(chara);
        if (p != null) {
            String temp = "%s %s\n"
                    + "HP: %d\n"
                    + "%s\n"
                    + "%s";

            String tipe = getType(chara);
            int hp = p.getHp();
            String strHidup = (p.alive == true) ? "Masih hidup" : "Sudah meninggal dunia dengan damai";
            String diet = (this.diet(p.getName()).equals("")) ? "Belum memakan siapa siapa" : "Memakan " + this.diet(p.getName());

            String info = String.format(temp, tipe, chara, hp, strHidup, diet);
            return info;
        } else {
            return "Tidak ada " + chara;
        }
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam
     * game
     *
     * @return String result nama dari semua character, format sesuai dengan
     * deskripsi soal atau contoh output
     */
    public String status() {
        String infos = "";
        for (Player p : player) {
            infos += status(p.getName()) + "\n";
        }

        return infos;
=======
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        return "";
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
       return "";
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        return "";        
>>>>>>> 41d825197b6b2c762feddfa80b1fd1ffa05197f6
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
<<<<<<< HEAD
     *
     * @param chara Player yang ingin ditampilkan seluruh history player yang
     * dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        String temp = "";

        Player p = find(chara);
        if (p != null) {
            String[] d = p.getDiet();

            for (int i = 0; i < d.length; i++) {
                if (i == d.length - 1) {
                    temp += d[i];
                } else {
                    temp += d[i] + ", ";
                }
            }

            return temp;
        } else {
            return "Tidak ada " + chara;
        }
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu
     * game
     *
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String infos = "";
        for (Player p : player) {
            infos += diet(p.getName()) + "\n";
        }

        return infos;
=======
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        return "";
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        return "";
>>>>>>> 41d825197b6b2c762feddfa80b1fd1ffa05197f6
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
<<<<<<< HEAD
     *
     * @param meName nama dari character yang sedang dimainkan
     * @param enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi
     * soal
     */
    public String attack(String meName, String enemyName) {
        String tmp = "Tidak ada %s atau %s";

        Player p1 = find(meName);
        Player p2 = find(enemyName);
        if ((p1 == null) || (p2 == null)) {
            return String.format(tmp, meName, enemyName);
        } else {
            return p1.attack(p2);
        }

    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya
     * boleh dilakukan oleh magician
     *
     * @param meName nama dari character yang sedang dimainkan
     * @param enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi
     * soal
     */
    public String burn(String meName, String enemyName) {
        String tmp = "Tidak ada %s atau %s";

        Player p1 = find(meName);
        Player p2 = find(enemyName);
        if ((p1 == null) || (p2 == null)) {
            return String.format(tmp, meName, enemyName);
        } else {
            return p1.burn(p2);
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa
     * dimakan sesuai dengan deskripsi yang ada di soal
     *
     * @param meName nama dari character yang sedang dimainkan
     * @param enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi
     * soal
     */
    public String eat(String meName, String enemyName) {
        String tmp = "Tidak ada %s atau %s";

        Player p1 = find(meName);
        Player p2 = find(enemyName);
        if ((p1 == null) || (p2 == null)) {
            return String.format(tmp, meName, enemyName);
        } else {
            if (p1.alive == true) {
                if (p1 instanceof Human) {
                    if ((p2.alive == false || p2.isBurnt()) && (p2 instanceof Monster)) {
                        String temp = "%s memakan %s\nNyawa %s kini %d";
                        remove(enemyName);
                        p1.increaseHP(15);

                        return String.format(temp, meName, enemyName, meName, p1.getHp());
                    } else {
                        return String.format("%s tidak bisa memakan %s", meName, enemyName);
                    }
                } else { //if Monster
                    if (p2.alive == false) {
                        String temp = "%s memakan %s\nNyawa %s kini %d";
                        remove(enemyName);
                        p1.increaseHP(15);

                        return String.format(temp, meName, enemyName, meName, p1.getHp());
                    } else {
                        return String.format("%s tidak bisa memakan %s", meName, enemyName);
                    }
                }
            } else {
                return String.format("%s tidak bisa memakan %s", meName, enemyName);
            }
        }
    }

    /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     *
     * @param meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai
     * deskripsi soal
     */
    public String roar(String meName) {
        Player p = find(meName);
        if (p != null) {
            if (p instanceof Monster) {
                return ((Monster) p).roar();
            } else {
                return meName + " tidak bisa berteriak";
            }

        } else {
            return "Tidak ada " + meName;
        }
    }

    public String getType(String name) {
        Player p = find(name);
        String x = "";
        if (p != null) {
            x = (p instanceof Human) ? "Human" : (p instanceof Magician) ? "Magician" : "Monster";
        }

        return x;
    }
}
=======
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        return "";
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        return "";
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        return "";
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        return "";
    }
}
>>>>>>> 41d825197b6b2c762feddfa80b1fd1ffa05197f6
