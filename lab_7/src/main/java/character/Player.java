package character;

<<<<<<< HEAD
public class Player {

    public boolean alive;
    public final String name;
    protected int hp;
    protected String diet = "";
    protected boolean burnt = false;

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
        this.alive = (hp != 0);
    }

    public String getName() {
        return this.name;
    }

    public String attack(Player target) {
        if (this.alive == true) {
            target.decreaseHP((target instanceof Magician) ? 20 : 10);

            if (target.getHp() == 0) {
                target.dead();
            }

            return "Nyawa " + target.getName() + " " + target.getHp();
        } else {
            return String.format("%s tidak bisa menyerang %s", this.getName(), target.getName());
        }
    }

    public boolean canEat(Player target) {
        if (this.alive == true) {
            if (this instanceof Human) {
                if ((this.alive == false || target.isBurnt()) && (target instanceof Monster)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return target.alive == false;
            }
        } else {
            return false;
        }
    }

    public String burn(Player target) {
        if (this.alive == true) {
            if (this instanceof Magician) {
                if (target.alive == true) {
                    target.decreaseHP((target instanceof Magician) ? 20 : 10);

                    if (target.getHp() == 0) {
                        target.dead(true);
                    }

                    String isburnt = (target.isBurnt() == true) ? "\n dan matang" : "";
                    return "Nyawa " + target.getName() + " " + target.getHp() + isburnt;
                } else {
                    target.dead(true);
                    String isburnt = (target.isBurnt() == true) ? "\n dan matang" : "";
                    return "Nyawa " + target.getName() + " " + target.getHp() + isburnt;
                }
            } else {
                return String.format("%s tidak bisa membakar %s", this.getName(), target.getName());
            }

        } else {
            return String.format("%s tidak bisa membakar %s", this.getName(), target.getName());
        }
    }

    public int getHp() {
        return this.hp;
    }

    public String[] getDiet() {
        String[] temp = this.diet.split(",");
        return temp;
    }

    public void decreaseHP(int hp) {
        if (this.hp < hp) {
            this.hp = 0;
        } else {
            this.hp -= hp;
        }
    }

    public void increaseHP(int hp) {
        this.hp += hp;
    }

    public void dead() {
        this.alive = false;
    }

    public void dead(boolean isBurnt) {
        this.alive = false;
        this.burnt = isBurnt;
    }

    public boolean isBurnt() {
        return this.burnt;
    }
}
=======
//  write Player Class here
>>>>>>> 41d825197b6b2c762feddfa80b1fd1ffa05197f6
